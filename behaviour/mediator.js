class User {
  constructor(name) {
    this.name = name
    this.room = null
  }

  send(message, to) {
    this.room.send(message, this, to)
  }

  receive(message, from) {
    console.log(`${from.name} => ${this.name}: ${message}`)
  }
}

class ChatRoom {
  constructor() {
    this.users = {}
  }

  register(user) {
    this.users[user.name] = user
    user.room = this
  }

  send(message, from, to) {
    if (to) {
      to.receive(message, from)
    } else {
      Object.keys(this.users).forEach(key => {
        if (this.users[key] !== from) {
          this.users[key].receive(message, from)
        }
      })
    }
  }
}

const arman = new User('Arman')
const anna = new User('Anna')
const karen = new User('Karen')

const room = new ChatRoom()

room.register(arman)
room.register(anna)
room.register(karen)

arman.send('Hello!', anna)
anna.send('Hello hello!', arman)
karen.send('Hi all')
