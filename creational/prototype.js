const car = {
  wheels: 4,

  init() {
    console.log(`es unem ${this.wheels} aniv, ${this.owner}y im tern e`)
  }
}

const carWithOwner = Object.create(car, {
  owner: {
    value: 'Arman'
  }
})

console.log(carWithOwner.__proto__ === car)

carWithOwner.init()
